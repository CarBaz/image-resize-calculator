ImageResizaCalculator.
Copyright (c) 2015, Carlos Bazaga.
Licensed under BSD 2-Clause.
All rights reserved.

A simple and easy to use image resize calculator.

Every field will make each other recalculate.


USAGE:

  First input your image original size then:
    � Set the scale factor to get new width ad height.
    � Set the new width or height to calculate the opposite and the needed scale factor.

Enjoy.

