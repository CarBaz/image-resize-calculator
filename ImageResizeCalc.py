#
# Written by: Carlos Bazaga
#
# Licensed under BSD 2-Clause
#
import Tkinter as tk
import math
import ttk

class Calculator_app(tk.Tk):
    """Class for a TK application to calculate proportions."""

    def __init__(self):
        """Return a new Coalculator_App."""
        tk.Tk.__init__(self)
        self.resizable(False,False)
        self.title("Image resize calculator.")

        width_validate = self.register(self._try_width)
        height_validate = self.register(self._try_height)
        scale_factor_validate = self.register(self._try_scale_factor)
        new_width_validate = self.register(self._try_new_width)
        new_height_validate = self.register(self._try_new_height)

        # Set variables.
        self.width_var      = tk.IntVar()
        self.height_var     = tk.IntVar()
        self.new_width_var  = tk.IntVar()
        self.new_height_var = tk.IntVar()
        self.factor_var     = tk.IntVar()

        # Create app frames.
        original_size_frame = ttk.Frame(self)
        original_width_frame = ttk.Frame(original_size_frame, padding=5)
        original_height_frame = ttk.Frame(original_size_frame, padding=5)

        functions_frame = ttk.Frame(self, padding=5)

        new_size_frame = ttk.Frame(self)
        new_width_frame = ttk.Frame(new_size_frame, padding=5)
        new_height_frame = ttk.Frame(new_size_frame, padding=5)

        clear_frame = ttk.Frame(self, padding=5)

        # Create data fields.
        self.width_field      = ttk.Entry(original_width_frame,  textvariable=self.width_var,      justify="center", validate='all', validatecommand=(width_validate, '%P'))
        self.height_field     = ttk.Entry(original_height_frame, textvariable=self.height_var,     justify="center", validate='all', validatecommand=(height_validate, '%P'))
        self.new_width_field  = ttk.Entry(new_width_frame,       textvariable=self.new_width_var,  justify="center", validate='all', validatecommand=(new_width_validate, '%P'))
        self.new_height_field = ttk.Entry(new_height_frame,      textvariable=self.new_height_var, justify="center", validate='all', validatecommand=(new_height_validate, '%P'))
        self.factor_field     = ttk.Entry(functions_frame,       textvariable=self.factor_var,     justify="center", validate='all', validatecommand=(scale_factor_validate, '%P'))

        self.width_field.select_range(0, tk.END)
        self.height_field.select_range(0, tk.END)
        self.new_width_field.select_range(0, tk.END)
        self.new_height_field.select_range(0, tk.END)
        self.factor_field.select_range(0, tk.END)

        # Create labels.
        ttk.Label(original_width_frame,  text="Width").pack(side="top")
        ttk.Label(original_height_frame, text="Height").pack(side="top")
        ttk.Label(new_width_frame,       text="New Width").pack(side="top")
        ttk.Label(new_height_frame,      text="New Height").pack(side="top")
        ttk.Label(functions_frame,       text="Scale Factor %").pack(side="top")

        # Pack elements.
        original_size_frame.pack(side="top", fill="x", expand=True)
        original_width_frame.pack(side="left", fill="y", expand=True)
        original_height_frame.pack(side="left", fill="y", expand=True)

        functions_frame.pack(side="top", fill="x", expand=True)

        new_size_frame.pack(side="top", fill="x", expand=True)
        new_width_frame.pack(side="left", fill="y", expand=True)
        new_height_frame.pack(side="left", fill="y", expand=True)

        clear_frame.pack(side="top", fill="x", expand=True)

        self.width_field.pack(side="top")
        self.height_field.pack(side="top")
        self.new_width_field.pack(side="top")
        self.new_height_field.pack(side="top")
        self.factor_field.pack(side="top")

        # Create reset button.
        ttk.Button(clear_frame, text="Reset", command=self._reset).pack(side="left", fill="x", expand=True)

        self._reset()

    def start(self):
        """Create the application window and run the Tk main loop."""
        self.mainloop()

    def _reset(self):
        """Set all values to default."""
        self.width_var.set(0)
        self.height_var.set(0)
        self.new_width_var.set(0)
        self.new_height_var.set(0)
        self.factor_var.set(100)

    def _refactor_v(self, scale, width, height):
        """Calculate new proportional sizes based on scale factor."""
        factor = scale / 100.
        self.new_width_var.set(int(width * factor))
        self.new_height_var.set(int(height * factor))

    def _rescale_width_v(self, new_height):
        """Calculate new proportional width and scale factor based on new height."""
        try:
            factor = float(new_height) / float(self.height_var.get())
            self.new_width_var.set(int(float(self.width_var.get()) * factor))
            self.factor_var.set(int(math.ceil(factor * 100)))
        except:
            pass

    def _rescale_height_v(self, new_width):
        """Calculate new proportional height and scale factor based on new width."""
        try:
            factor = float(new_width) / float(self.width_var.get())
            self.new_height_var.set(int(float(self.height_var.get()) * factor))
            self.factor_var.set(int(math.ceil(factor * 100)))
        except:
            pass

    def _try_width(self, postval):
        """Callback Function to check original size entry fields."""
        try:
            val = int(postval)
        except:
            if postval == '':
                self.width_var.set(0)
                return True
            else:
                return False
        else:
            if 0 <= val:
                self._refactor_v(self.factor_var.get(), val, self.height_var.get())
                return True
            else:
                return False

    def _try_height(self, postval):
        """Callback Function to check original size entry fields."""
        try:
            val = int(postval)
        except:
            if postval == '':
                self.height_var.set(0)
                return True
            else:
                return False
        else:
            if 0 <= val:
                self._refactor_v(self.factor_var.get(), self.width_var.get(), val)
                return True
            else:
                return False

    def _try_scale_factor(self, postval):
        """Callback Function to check scale factor field."""
        try:
            val = int(postval)
        except:
            if postval == '':
                self.factor_var.set(0)
                return True
            else:
                return False
        else:
            if 0 <= val:
                self._refactor_v(val, self.width_var.get(), self.height_var.get())
                return True
            else:
                return False

    def _try_new_width(self, postval):
        """Callback Function to check width entry field."""
        try:
            val = int(postval)
        except:
            if postval == '':
                return True
            else:
                return False
        else:
            self._rescale_height_v(val)
            return True

    def _try_new_height(self, postval):
        """Callback Function to check height entry field."""
        try:
            val = int(postval)
        except:
            if postval == '':
                return True
            else:
                return False
        else:
            self._rescale_width_v(val)
            return True

if __name__ == "__main__":
    calculator = Calculator_app()
    calculator.start()